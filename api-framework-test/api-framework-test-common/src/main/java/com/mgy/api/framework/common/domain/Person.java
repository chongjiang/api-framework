package com.mgy.api.framework.common.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Person
 *
 * @author mgy
 * @date 2019.10.20
 */
@ApiModel
@Data
public class Person {
    @ApiModelProperty(value = "姓名",name = "name",example = "张三")
    private String name;
    @ApiModelProperty(value = "年龄",example = "10")
    private Integer age;
    @ApiModelProperty(value = "性别",example = "F")
    private char sex;
}
