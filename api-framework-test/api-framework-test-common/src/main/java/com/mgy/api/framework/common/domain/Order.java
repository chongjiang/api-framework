package com.mgy.api.framework.common.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.Date;

@Data
public class Order {

    /**
     * 手工调整金额.格式为:1.01;单位:元;精确到小数点后两位.
     */
    @JSONField(name = "adjust_fee")
    private String adjustFee;

    /**
     * assemblyItem
     */
    @JSONField(name = "assembly_item")
    private String assemblyItem;

    /**
     * 价格
     */
    @JSONField(name = "assembly_price")
    private String assemblyPrice;

    /**
     * 主商品订单id
     */
    @JSONField(name = "assembly_rela")
    private String assemblyRela;

    /**
     * 捆绑的子订单号，表示该子订单要和捆绑的子订单一起发货，用于卖家子订单捆绑发货
     */
    @JSONField(name = "bind_oid")
    private Long bindOid;

    /**
     * bind_oid字段的升级，支持返回绑定的多个子订单，多个子订单以半角逗号分隔
     */
    @JSONField(name = "bind_oids")
    private String bindOids;

    /**
     * 买家昵称
     */
    @JSONField(name = "buyer_nick")
    private String buyerNick;

    /**
     * 买家是否已评价。可选值：true(已评价)，false(未评价)
     */
    @JSONField(name = "buyer_rate")
    private Boolean buyerRate;

    /**
     * calPenalty
     */
    @JSONField(name = "cal_penalty")
    private String calPenalty;

    /**
     * carStoreCode
     */
    @JSONField(name = "car_store_code")
    private String carStoreCode;

    /**
     * carStoreName
     */
    @JSONField(name = "car_store_name")
    private String carStoreName;

    /**
     * carTaker
     */
    @JSONField(name = "car_taker")
    private String carTaker;

    /**
     * carTakerID
     */
    @JSONField(name = "car_taker_id")
    private String carTakerId;

    /**
     * carTakerIDNum
     */
    @JSONField(name = "car_taker_id_num")
    private String carTakerIdNum;

    /**
     * carTakerPhone
     */
    @JSONField(name = "car_taker_phone")
    private String carTakerPhone;

    /**
     * 交易商品对应的类目ID
     */
    @JSONField(name = "cid")
    private Long cid;

    /**
     * clCarTaker
     */
    @JSONField(name = "cl_car_taker")
    private String clCarTaker;

    /**
     * clCarTakerIDNum
     */
    @JSONField(name = "cl_car_taker_i_d_num")
    private String clCarTakerIDNum;

    /**
     * clCarTakerIDNum
     */
    @JSONField(name = "cl_car_taker_id_num")
    private String clCarTakerIdNum;

    /**
     * clCarTakerPhone
     */
    @JSONField(name = "cl_car_taker_phone")
    private String clCarTakerPhone;

    /**
     * clDownPayment
     */
    @JSONField(name = "cl_down_payment")
    private String clDownPayment;

    /**
     * clDownPaymentRatio
     */
    @JSONField(name = "cl_down_payment_ratio")
    private String clDownPaymentRatio;

    /**
     * clInstallmentNum
     */
    @JSONField(name = "cl_installment_num")
    private String clInstallmentNum;

    /**
     * clMonthPayment
     */
    @JSONField(name = "cl_month_payment")
    private String clMonthPayment;

    /**
     * clServiceFee
     */
    @JSONField(name = "cl_service_fee")
    private String clServiceFee;

    /**
     * clTailPayment
     */
    @JSONField(name = "cl_tail_payment")
    private String clTailPayment;

    /**
     * 天猫搭配宝
     */
    @JSONField(name = "combo_id")
    private String comboId;

    /**
     * 子订单发货时间，当卖家对订单进行了多次发货，子订单的发货时间和主订单的发货时间可能不一样了，那么就需要以子订单的时间为准。（没有进行多次发货的订单，主订单的发货时间和子订单的发货时间都一样）
     */
    @JSONField(name = "consign_time")
    private String consignTime;

    /**
     * 定制信息
     */
    @JSONField(name = "customization")
    private String customization;

    /**
     * 子订单级订单优惠金额。精确到2位小数;单位:元。如:200.07，表示:200元7分
     */
    @JSONField(name = "discount_fee")
    private String discountFee;

    /**
     * 分摊之后的实付金额
     */
    @JSONField(name = "divide_order_fee")
    private String divideOrderFee;

    /**
     * downPayment
     */
    @JSONField(name = "down_payment")
    private String downPayment;

    /**
     * downPaymentRatio
     */
    @JSONField(name = "down_payment_ratio")
    private String downPaymentRatio;

    /**
     * 子订单的交易结束时间说明：子订单有单独的结束时间，与主订单的结束时间可能有所不同，在有退款发起的时候或者是主订单分阶段付款的时候，子订单的结束时间会早于主订单的结束时间，所以开放这个字段便于订单结束状态的判断
     */
    @JSONField(name = "end_time")
    private Date endTime;

    /**
     * 子订单预计发货时间
     */
    @JSONField(name = "estimate_con_time")
    private String estimateConTime;

    /**
     * 车牌号码
     */
    @JSONField(name = "et_plate_number")
    private String etPlateNumber;

    /**
     * 天猫汽车服务预约时间
     */
    @JSONField(name = "et_ser_time")
    private String etSerTime;

    /**
     * 电子凭证预约门店地址
     */
    @JSONField(name = "et_shop_name")
    private String etShopName;

    /**
     * 电子凭证核销门店地址
     */
    @JSONField(name = "et_verified_shop_name")
    private String etVerifiedShopName;

    /**
     * 订单履行状态，如喵鲜生极速达：分单完成
     */
    @JSONField(name = "f_status")
    private String fStatus;

    /**
     * 单履行内容，如喵鲜生极速达：storeId,phone
     */
    @JSONField(name = "f_term")
    private String fTerm;

    /**
     * 订单履行类型，如喵鲜生极速达（jsd）
     */
    @JSONField(name = "f_type")
    private String fType;

    /**
     * 花呗分期期数
     */
    @JSONField(name = "fqg_num")
    private Long fqgNum;

    /**
     * 商品的字符串编号(注意：iid近期即将废弃，请用num_iid参数)
     */
    @JSONField(name = "iid")
    private String iid;

    /**
     * installmentNum
     */
    @JSONField(name = "installment_num")
    private String installmentNum;

    /**
     * 库存类型：6为在途
     */
    @JSONField(name = "inv_type")
    private String invType;

    /**
     * 子订单所在包裹的运单号
     */
    @JSONField(name = "invoice_no")
    private String invoiceNo;

    /**
     * 表示订单交易是否含有对应的代销采购单。如果该订单中存在一个对应的代销采购单，那么该值为true；反之，该值为false。
     */
    @JSONField(name = "is_daixiao")
    private Boolean isDaixiao;

    /**
     * 是否商家承担手续费
     */
    @JSONField(name = "is_fqg_s_fee")
    private Boolean isFqgSFee;

    /**
     * 是否超卖
     */
    @JSONField(name = "is_oversold")
    private Boolean isOversold;

    /**
     * 是否是服务订单，是返回true，否返回false。
     */
    @JSONField(name = "is_service_order")
    private Boolean isServiceOrder;

    /**
     * 是否发货
     */
    @JSONField(name = "is_sh_ship")
    private Boolean isShShip;

    /**
     * 子订单是否是www订单
     */
    @JSONField(name = "is_www")
    private Boolean isWww;

    /**
     * 套餐ID
     */
    @JSONField(name = "item_meal_id")
    private Long itemMealId;

    /**
     * 套餐的值。如：M8原装电池:便携支架:M8专用座充:莫凡保护袋
     */
    @JSONField(name = "item_meal_name")
    private String itemMealName;

    /**
     * 商品备注
     */
    @JSONField(name = "item_memo")
    private String itemMemo;

    /**
     * 子订单发货的快递公司名称
     */
    @JSONField(name = "logistics_company")
    private String logisticsCompany;

    /**
     * 免单金额
     */
    @JSONField(name = "md_fee")
    private String mdFee;

    /**
     * 免单资格属性
     */
    @JSONField(name = "md_qualification")
    private String mdQualification;

    /**
     * 订单修改时间，目前只有taobao.trade.ordersku.update会返回此字段。
     */
    @JSONField(name = "modified")
    private Date modified;

    /**
     * monthPayment
     */
    @JSONField(name = "month_payment")
    private String monthPayment;

    /**
     * 购买数量。取值范围:大于零的整数
     */
    @JSONField(name = "num")
    private Long num;

    /**
     * 商品数字ID
     */
    @JSONField(name = "num_iid")
    private Long numIid;

    /**
     *
     */
    @JSONField(name = "o2o_et_order_id")
    private String o2oEtOrderId;

    /**
     * 子订单编号
     */
    @JSONField(name = "oid")
    private Long oid;

    /**
     * oidStr
     */
    @JSONField(name = "oid_str")
    private String oidStr;

    /**
     * top动态字段
     */
    @JSONField(name = "order_attr")
    private String orderAttr;

    /**
     * 子订单来源,如jhs(聚划算)、taobao(淘宝)、wap(无线)
     */
    @JSONField(name = "order_from")
    private String orderFrom;

    /**
     * outUniqueId
     */
    @JSONField(name = "out_unique_id")
    private String outUniqueId;

    /**
     * 商家外部编码(可与商家外部系统对接)。外部商家自己定义的商品Item的id，可以通过taobao.items.custom.get获取商品的Item的信息
     */
    @JSONField(name = "outer_iid")
    private String outerIid;

    /**
     * 外部网店自己定义的Sku编号
     */
    @JSONField(name = "outer_sku_id")
    private String outerSkuId;

    /**
     * 优惠分摊
     */
    @JSONField(name = "part_mjz_discount")
    private String partMjzDiscount;

    /**
     * 子订单实付金额。精确到2位小数，单位:元。如:200.07，表示:200元7分。对于多子订单的交易，计算公式如下：payment = price * num + adjust_fee - discount_fee ；单子订单交易，payment与主订单的payment一致，对于退款成功的子订单，由于主订单的优惠分摊金额，会造成该字段可能不为0.00元。建议使用退款前的实付金额减去退款单中的实际退款金额计算。
     */
    @JSONField(name = "payment")
    private String payment;

    /**
     * penalty
     */
    @JSONField(name = "penalty")
    private String penalty;

    /**
     * 商品图片的绝对路径
     */
    @JSONField(name = "pic_path")
    private String picPath;

    /**
     * 商品价格。精确到2位小数;单位:元。如:200.07，表示:200元7分
     */
    @JSONField(name = "price")
    private String price;

    /**
     * 最近退款ID
     */
    @JSONField(name = "refund_id")
    private Long refundId;

    /**
     * 退款状态。退款状态。可选值 WAIT_SELLER_AGREE(买家已经申请退款，等待卖家同意) WAIT_BUYER_RETURN_GOODS(卖家已经同意退款，等待买家退货) WAIT_SELLER_CONFIRM_GOODS(买家已经退货，等待卖家确认收货) SELLER_REFUSE_BUYER(卖家拒绝退款) CLOSED(退款关闭) SUCCESS(退款成功)
     */
    @JSONField(name = "refund_status")
    private String refundStatus;

    /**
     * 卖家昵称
     */
    @JSONField(name = "seller_nick")
    private String sellerNick;

    /**
     * 卖家是否已评价。可选值：true(已评价)，false(未评价)
     */
    @JSONField(name = "seller_rate")
    private Boolean sellerRate;

    /**
     * 卖家类型，可选值为：B（商城商家），C（普通卖家）
     */
    @JSONField(name = "seller_type")
    private String sellerType;

    /**
     * serviceFee
     */
    @JSONField(name = "service_fee")
    private String serviceFee;

    /**
     * 仓储信息
     */
    @JSONField(name = "shipper")
    private String shipper;

    /**
     * 子订单的运送方式（卖家对订单进行多次发货之后，一个主订单下的子订单的运送方式可能不同，用order.shipping_type来区分子订单的运送方式）
     */
    @JSONField(name = "shipping_type")
    private String shippingType;

    /**
     * 商品的最小库存单位Sku的id.可以通过taobao.item.sku.get获取详细的Sku信息
     */
    @JSONField(name = "sku_id")
    private String skuId;

    /**
     * SKU的值。如：机身颜色:黑色;手机套餐:官方标配
     */
    @JSONField(name = "sku_properties_name")
    private String skuPropertiesName;

    /**
     * 订单快照详细信息
     */
    @JSONField(name = "snapshot")
    private String snapshot;

    /**
     * 订单快照URL
     */
    @JSONField(name = "snapshot_url")
    private String snapshotUrl;

    /**
     * 订单状态（请关注此状态，如果为TRADE_CLOSED_BY_TAOBAO状态，则不要对此订单进行发货，切记啊！）。可选值: <ul><li>TRADE_NO_CREATE_PAY(没有创建支付宝交易) </li><li>WAIT_BUYER_PAY(等待买家付款) </li><li>WAIT_SELLER_SEND_GOODS(等待卖家发货,即:买家已付款) </li><li>WAIT_BUYER_CONFIRM_GOODS(等待买家确认收货,即:卖家已发货) </li><li>TRADE_BUYER_SIGNED(买家已签收,货到付款专用) </li><li>TRADE_FINISHED(交易成功) </li><li>TRADE_CLOSED(付款以后用户退款成功，交易自动关闭) </li><li>TRADE_CLOSED_BY_TAOBAO(付款以前，卖家或买家主动关闭交易)</li><li>PAY_PENDING(国际信用卡支付付款确认中)</li></ul>
     */
    @JSONField(name = "status")
    private String status;

    /**
     * 发货的仓库编码
     */
    @JSONField(name = "store_code")
    private String storeCode;

    /**
     * 天猫国际官网直供子订单关税税费
     */
    @JSONField(name = "sub_order_tax_fee")
    private String subOrderTaxFee;

    /**
     * 天猫国际子订单计税优惠金额
     */
    @JSONField(name = "sub_order_tax_promotion_fee")
    private String subOrderTaxPromotionFee;

    /**
     * 天猫国际官网直供子订单关税税率
     */
    @JSONField(name = "sub_order_tax_rate")
    private String subOrderTaxRate;

    /**
     * tailPayment
     */
    @JSONField(name = "tail_payment")
    private String tailPayment;

    /**
     * 天猫国际订单包税金额
     */
    @JSONField(name = "tax_coupon_discount")
    private String taxCouponDiscount;

    /**
     * 天猫国际订单是否包税
     */
    @JSONField(name = "tax_free")
    private Boolean taxFree;

    /**
     * 门票有效期的key
     */
    @JSONField(name = "ticket_expdate_key")
    private String ticketExpdateKey;

    /**
     * 对应门票有效期的外部id
     */
    @JSONField(name = "ticket_outer_id")
    private String ticketOuterId;

    /**
     * 订单超时到期时间。格式:yyyy-MM-dd HH:mm:ss
     */
    @JSONField(name = "timeout_action_time")
    private Date timeoutActionTime;

    /**
     * 商品标题
     */
    @JSONField(name = "title")
    private String title;

    /**
     * 支持家装类物流的类型
     */
    @JSONField(name = "tmser_spu_code")
    private String tmserSpuCode;

    /**
     * 应付金额（商品价格 * 商品数量 + 手工调整金额 - 子订单级订单优惠金额）。精确到2位小数;单位:元。如:200.07，表示:200元7分
     */
    @JSONField(name = "total_fee")
    private String totalFee;

    /**
     * 交易类型
     */
    @JSONField(name = "type")
    private String type;

    /**
     * wsBankApplyNo
     */
    @JSONField(name = "ws_bank_apply_no")
    private String wsBankApplyNo;

    /**
     * xxx
     */
    @JSONField(name = "xxx")
    private String xxx;

    /**
     * 征集预售订单征集状态：1（征集中），2（征集成功），3（征集失败）
     */
    @JSONField(name = "zhengji_status")
    private String zhengjiStatus;

}
