package com.mgy.api.framework.common.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class Trade {

    /**
     * Acookie订单唯一ID。
     */
    @JSONField(name = "acookie_id")
    private String acookieId;

    /**
     * 卖家手工调整金额，精确到2位小数，单位：元。如：200.07，表示：200元7分。来源于订单价格修改，如果有多笔子订单的时候，这个为0，单笔的话则跟[order].adjust_fee一样
     */
    @JSONField(name = "adjust_fee")
    private String adjustFee;

    /**
     * 买家的支付宝id号，在UIC中有记录，买家支付宝的唯一标示，不因为买家更换Email账号而改变。
     */
    @JSONField(name = "alipay_id")
    private Long alipayId;

    /**
     * 支付宝交易号，如：2009112081173831
     */
    @JSONField(name = "alipay_no")
    private String alipayNo;

    /**
     * 付款时使用的支付宝积分的额度,单位分，比如返回1，则为1分钱
     */
    @JSONField(name = "alipay_point")
    private Long alipayPoint;

    /**
     * 创建交易接口成功后，返回的支付url
     */
    @JSONField(name = "alipay_url")
    private String alipayUrl;

    /**
     * 淘宝下单成功了,但由于某种错误支付宝订单没有创建时返回的信息。taobao.trade.add接口专用
     */
    @JSONField(name = "alipay_warn_msg")
    private String alipayWarnMsg;

    /**
     * 区域id，代表订单下单的区位码，区位码是通过省市区转换而来，通过区位码能精确到区内的划分，比如310012是杭州市西湖区华星路
     */
    @JSONField(name = "area_id")
    private String areaId;

    /**
     * 物流到货时效截单时间，格式 HH:mm
     */
    @JSONField(name = "arrive_cut_time")
    private String arriveCutTime;

    /**
     * 物流到货时效，单位天
     */
    @JSONField(name = "arrive_interval")
    private Long arriveInterval;

    /**
     * 组合商品
     */
    @JSONField(name = "assembly")
    private String assembly;

    /**
     * 同步到卖家库的时间，taobao.trades.sold.incrementv.get接口返回此字段
     */
    @JSONField(name = "async_modified")
    private Date asyncModified;

    /**
     * 交易中剩余的确认收货金额（这个金额会随着子订单确认收货而不断减少，交易成功后会变为零）。精确到2位小数;单位:元。如:200.07，表示:200 元7分
     */
    @JSONField(name = "available_confirm_fee")
    private String availableConfirmFee;

    /**
     * 买家支付宝账号
     */
    @JSONField(name = "buyer_alipay_no")
    private String buyerAlipayNo;

    /**
     * 买家下单的地区
     */
    @JSONField(name = "buyer_area")
    private String buyerArea;

    /**
     * 买家货到付款服务费。精确到2位小数;单位:元。如:12.07，表示:12元7分
     */
    @JSONField(name = "buyer_cod_fee")
    private String buyerCodFee;

    /**
     * 买家邮件地址
     */
    @JSONField(name = "buyer_email")
    private String buyerEmail;

    /**
     * 买家备注旗帜（与淘宝网上订单的买家备注旗帜对应，只有买家才能查看该字段）红、黄、绿、蓝、紫 分别对应 1、2、3、4、5
     */
    @JSONField(name = "buyer_flag")
    private Long buyerFlag;

    /**
     * 买家下单的IP信息，仅供taobao.trade.fullinfo.get查询返回。需要对返回结果进行Base64解码。
     */
    @JSONField(name = "buyer_ip")
    private String buyerIp;

    /**
     * 买家备注（与淘宝网上订单的买家备注对应，只有买家才能查看该字段）
     */
    @JSONField(name = "buyer_memo")
    private String buyerMemo;

    /**
     * 买家留言
     */
    @JSONField(name = "buyer_message")
    private String buyerMessage;

    /**
     * 买家昵称
     */
    @JSONField(name = "buyer_nick")
    private String buyerNick;

    /**
     * 买家获得积分,返点的积分。格式:100;单位:个。返点的积分要交易成功之后才能获得。
     */
    @JSONField(name = "buyer_obtain_point_fee")
    private Long buyerObtainPointFee;

    /**
     * 买家是否已评价。可选值:true(已评价),false(未评价)。如买家只评价未打分，此字段仍返回false
     */
    @JSONField(name = "buyer_rate")
    private Boolean buyerRate;

    /**
     * 买家可以通过此字段查询是否当前交易可以评论，can_rate=true可以评价，false则不能评价。
     */
    @JSONField(name = "can_rate")
    private Boolean canRate;

    /**
     * 货到付款服务费。精确到2位小数;单位:元。如:12.07，表示:12元7分。
     */
    @JSONField(name = "cod_fee")
    private String codFee;

    /**
     * 货到付款物流状态。初始状态 NEW_CREATED,接单成功 ACCEPTED_BY_COMPANY,接单失败 REJECTED_BY_COMPANY,接单超时 RECIEVE_TIMEOUT,揽收成功 TAKEN_IN_SUCCESS,揽收失败 TAKEN_IN_FAILED,揽收超时 TAKEN_TIMEOUT,签收成功 SIGN_IN,签收失败 REJECTED_BY_OTHER_SIDE,订单等待发送给物流公司 WAITING_TO_BE_SENT,用户取消物流订单 CANCELED
     */
    @JSONField(name = "cod_status")
    private String codStatus;

    /**
     * 交易佣金。精确到2位小数;单位:元。如:200.07，表示:200元7分
     */
    @JSONField(name = "commission_fee")
    private String commissionFee;

    /**
     * 物流发货时效，单位小时
     */
    @JSONField(name = "consign_interval")
    private Long consignInterval;

    /**
     * 卖家发货时间。格式:yyyy-MM-dd HH:mm:ss
     */
    @JSONField(name = "consign_time")
    private Date consignTime;

    /**
     * 订单中使用红包付款的金额
     */
    @JSONField(name = "coupon_fee")
    private Long couponFee;

    /**
     * 交易创建时间。格式:yyyy-MM-dd HH:mm:ss
     */
    @JSONField(name = "created")
    private Date created;

    /**
     * 使用信用卡支付金额数
     */
    @JSONField(name = "credit_card_fee")
    private String creditCardFee;

    /**
     * 跨境订单
     */
    @JSONField(name = "cross_bonded_declare")
    private Boolean crossBondedDeclare;

    /**
     * 聚划算火拼标记
     */
    @JSONField(name = "delay_create_delivery")
    private Long delayCreateDelivery;

    /**
     * 可以使用trade.promotion_details查询系统优惠系统优惠金额（如打折，VIP，满就送等），精确到2位小数，单位：元。如：200.07，表示：200元7分
     */
    @JSONField(name = "discount_fee")
    private String discountFee;

    /**
     * (encrypt)买家的支付宝id号，在UIC中有记录，买家支付宝的唯一标示，不因为买家更换Email账号而改变。
     */
    @JSONField(name = "encrypt_alipay_id")
    private String encryptAlipayId;

    /**
     * 交易结束时间。交易成功时间(更新交易状态为成功的同时更新)/确认收货时间或者交易关闭时间 。格式:yyyy-MM-dd HH:mm:ss
     */
    @JSONField(name = "end_time")
    private Date endTime;

    /**
     * 时间
     */
    @JSONField(name = "es_date")
    private String esDate;

    /**
     * 时间段
     */
    @JSONField(name = "es_range")
    private String esRange;

    /**
     * 商家的预计发货时间
     */
    @JSONField(name = "est_con_time")
    private String estConTime;

    /**
     * 车牌号码
     */
    @JSONField(name = "et_plate_number")
    private String etPlateNumber;

    /**
     * 天猫汽车服务预约时间
     */
    @JSONField(name = "et_ser_time")
    private String etSerTime;

    /**
     * 扫码购关联门店
     */
    @JSONField(name = "et_shop_id")
    private Long etShopId;

    /**
     * 电子凭证预约门店地址
     */
    @JSONField(name = "et_shop_name")
    private String etShopName;

    /**
     * 电子凭证扫码购-扫码支付订单type
     */
    @JSONField(name = "et_type")
    private String etType;

    /**
     * 电子凭证核销门店地址
     */
    @JSONField(name = "et_verified_shop_name")
    private String etVerifiedShopName;

    /**
     * 电子凭证的垂直信息
     */
    @JSONField(name = "eticket_ext")
    private String eticketExt;

    /**
     * 天猫电子凭证家装
     */
    @JSONField(name = "eticket_service_addr")
    private String eticketServiceAddr;

    /**
     * 快递代收款。精确到2位小数;单位:元。如:212.07，表示:212元7分
     */
    @JSONField(name = "express_agency_fee")
    private String expressAgencyFee;

    /**
     * 聚划算一起买字段
     */
    @JSONField(name = "forbid_consign")
    private Long forbidConsign;

    /**
     * 判断订单是否有买家留言，有买家留言返回true，否则返回false
     */
    @JSONField(name = "has_buyer_message")
    private Boolean hasBuyerMessage;

    /**
     * 是否包含邮费。与available_confirm_fee同时使用。可选值:true(包含),false(不包含)
     */
    @JSONField(name = "has_post_fee")
    private Boolean hasPostFee;

    /**
     * 订单中是否包含运费险订单，如果包含运费险订单返回true，不包含运费险订单，返回false
     */
    @JSONField(name = "has_yfx")
    private Boolean hasYfx;

    /**
     * 出生日期
     */
    @JSONField(name = "hk_birthday")
    private String hkBirthday;

    /**
     * 证件号码
     */
    @JSONField(name = "hk_card_code")
    private String hkCardCode;

    /**
     * 证件类型001代表港澳通行证类型、002代表入台证003代表护照
     */
    @JSONField(name = "hk_card_type")
    private String hkCardType;

    /**
     * 中文名
     */
    @JSONField(name = "hk_china_name")
    private String hkChinaName;

    /**
     * 拼音名
     */
    @JSONField(name = "hk_en_name")
    private String hkEnName;

    /**
     * 航班飞行时间
     */
    @JSONField(name = "hk_flight_date")
    private String hkFlightDate;

    /**
     * 航班号
     */
    @JSONField(name = "hk_flight_no")
    private String hkFlightNo;

    /**
     * 性别M: 男性，F: 女性
     */
    @JSONField(name = "hk_gender")
    private String hkGender;

    /**
     * 提货地点
     */
    @JSONField(name = "hk_pickup")
    private String hkPickup;

    /**
     * 提货地点id
     */
    @JSONField(name = "hk_pickup_id")
    private String hkPickupId;

    /**
     * 采购订单标识
     */
    @JSONField(name = "identity")
    private String identity;

    /**
     * 商品字符串编号(注意：iid近期即将废弃，请用num_iid参数)
     */
    @JSONField(name = "iid")
    private String iid;

    /**
     * 发票类型（ 1 电子发票 2  纸质发票 ）
     */
    @JSONField(name = "invoice_kind")
    private String invoiceKind;

    /**
     * 发票抬头
     */
    @JSONField(name = "invoice_name")
    private String invoiceName;

    /**
     * 发票类型
     */
    @JSONField(name = "invoice_type")
    private String invoiceType;

    /**
     * 是否3D交易
     */
    @JSONField(name = "is_3D")
    private Boolean is3D;

    /**
     * 表示是否是品牌特卖（常规特卖，不包括特卖惠和特实惠）订单，如果是返回true，如果不是返回false。当此字段与is_force_wlb均为true时，订单强制物流宝发货。
     */
    @JSONField(name = "is_brand_sale")
    private Boolean isBrandSale;

    /**
     * 表示订单交易是否含有对应的代销采购单。如果该订单中存在一个对应的代销采购单，那么该值为true；反之，该值为false。
     */
    @JSONField(name = "is_daixiao")
    private Boolean isDaixiao;

    /**
     * 订单是否强制使用物流宝发货。当此字段与is_brand_sale均为true时，订单强制物流宝发货。此字段为false时，该订单根据流转规则设置可以使用物流宝或者常规方式发货
     */
    @JSONField(name = "is_force_wlb")
    private Boolean isForceWlb;

    /**
     * 是否保障速递，如果为true，则为保障速递订单，使用线下联系发货接口发货，如果未false，则该订单非保障速递，根据卖家设置的订单流转规则可使用物流宝或者常规物流发货。
     */
    @JSONField(name = "is_lgtype")
    private Boolean isLgtype;

    /**
     * 是否是多次发货的订单如果卖家对订单进行多次发货，则为true否则为false
     */
    @JSONField(name = "is_part_consign")
    private Boolean isPartConsign;

    /**
     * 是否屏蔽发货
     */
    @JSONField(name = "is_sh_ship")
    private Boolean isShShip;

    /**
     * 表示订单交易是否网厅订单。 如果该订单是网厅订单，那么该值为true；反之，该值为false。
     */
    @JSONField(name = "is_wt")
    private Boolean isWt;

    /**
     * 次日达订单送达时间
     */
    @JSONField(name = "lg_aging")
    private String lgAging;

    /**
     * 次日达，三日达等送达类型
     */
    @JSONField(name = "lg_aging_type")
    private String lgAgingType;

    /**
     * 订单出现异常问题的时候，给予用户的描述,没有异常的时候，此值为空
     */
    @JSONField(name = "mark_desc")
    private String markDesc;

    /**
     * 垂直市场
     */
    @JSONField(name = "market")
    private String market;

    /**
     * 交易修改时间(用户对订单的任何修改都会更新此字段)。格式:yyyy-MM-dd HH:mm:ss
     */
    @JSONField(name = "modified")
    private Date modified;

    /**
     * 商品购买数量。取值范围：大于零的整数,对于一个trade对应多个order的时候（一笔主订单，对应多笔子订单），num=0，num是一个跟商品关联的属性，一笔订单对应多比子订单的时候，主订单上的num无意义。
     */
    @JSONField(name = "num")
    private Long num;

    /**
     * 商品数字编号
     */
    @JSONField(name = "num_iid")
    private Long numIid;

    /**
     * 卡易售垂直表信息，去除下单ip之后的结果
     */
    @JSONField(name = "nut_feature")
    private String nutFeature;

    /**
     * 导购宝=crm
     */
    @JSONField(name = "o2o")
    private String o2o;

    /**
     * o2oContract
     */
    @JSONField(name = "o2o_contract")
    private String o2oContract;

    /**
     * 导购宝提货方式，inshop：店内提货，online：线上发货
     */
    @JSONField(name = "o2o_delivery")
    private String o2oDelivery;

    /**
     * 分阶段交易的特权定金订单ID
     */
    @JSONField(name = "o2o_et_order_id")
    private String o2oEtOrderId;

    /**
     * 导购员id
     */
    @JSONField(name = "o2o_guide_id")
    private String o2oGuideId;

    /**
     * 导购员名称
     */
    @JSONField(name = "o2o_guide_name")
    private String o2oGuideName;

    /**
     * 外部订单号
     */
    @JSONField(name = "o2o_out_trade_id")
    private String o2oOutTradeId;

    /**
     * o2oServiceAddress
     */
    @JSONField(name = "o2o_service_address")
    private String o2oServiceAddress;

    /**
     * o2oServiceCity
     */
    @JSONField(name = "o2o_service_city")
    private String o2oServiceCity;

    /**
     * o2oServiceDistrict
     */
    @JSONField(name = "o2o_service_district")
    private String o2oServiceDistrict;

    /**
     * o2oServiceMobile
     */
    @JSONField(name = "o2o_service_mobile")
    private String o2oServiceMobile;

    /**
     * o2oServiceName
     */
    @JSONField(name = "o2o_service_name")
    private String o2oServiceName;

    /**
     * o2oServiceState
     */
    @JSONField(name = "o2o_service_state")
    private String o2oServiceState;

    /**
     * o2oServiceTown
     */
    @JSONField(name = "o2o_service_town")
    private String o2oServiceTown;

    /**
     * 导购员门店id
     */
    @JSONField(name = "o2o_shop_id")
    private String o2oShopId;

    /**
     * 导购门店名称
     */
    @JSONField(name = "o2o_shop_name")
    private String o2oShopName;

    /**
     * 抢单状态0,未处理待分发;1,抢单中;2,已抢单;3,已发货;-1,超时;-2,处理异常;-3,匹配失败;-4,取消抢单;-5,退款取消;-9,逻辑删除
     */
    @JSONField(name = "o2o_snatch_status")
    private String o2oSnatchStatus;

    /**
     * 特权定金订单的尾款订单ID
     */
    @JSONField(name = "o2o_step_order_id")
    private String o2oStepOrderId;

    /**
     * 组装O2O多阶段尾款订单的明细数据 总阶段数，当前阶数，阶段金额（单位：分），支付状态，例如 3_1_100_paid ; 3_2_2000_nopaid
     */
    @JSONField(name = "o2o_step_trade_detail")
    private String o2oStepTradeDetail;

    /**
     * o2oStepTradeDetailNew
     */
    @JSONField(name = "o2o_step_trade_detail_new")
    private String o2oStepTradeDetailNew;

    /**
     * 分阶段订单的特权定金抵扣金额，单位：分
     */
    @JSONField(name = "o2o_voucher_price")
    private String o2oVoucherPrice;

    /**
     * o2oXiaopiao
     */
    @JSONField(name = "o2o_xiaopiao")
    private String o2oXiaopiao;

    /**
     * 门店预约自提订单标
     */
    @JSONField(name = "obs")
    private String obs;

    /**
     * 天猫国际拦截
     */
    @JSONField(name = "ofp_hold")
    private Long ofpHold;

    /**
     * 星盘标识字段
     */
    @JSONField(name = "omni_attr")
    private String omniAttr;

    /**
     * 星盘业务字段
     */
    @JSONField(name = "omni_param")
    private String omniParam;

    /**
     * 全渠道商品通相关字段
     */
    @JSONField(name = "omnichannel_param")
    private String omnichannelParam;

    /**
     * 天猫国际官网直供主订单关税税费
     */
    @JSONField(name = "order_tax_fee")
    private String orderTaxFee;

    /**
     * 天猫国际计税优惠金额
     */
    @JSONField(name = "order_tax_promotion_fee")
    private String orderTaxPromotionFee;

    /**
     * 订单列表
     */
    @JSONField(name = "orders")
    private List<Order> orders;

    /**
     * 时间
     */
    @JSONField(name = "os_date")
    private String osDate;

    /**
     * 时间段
     */
    @JSONField(name = "os_range")
    private String osRange;

    /**
     * 满返红包的金额；如果没有满返红包，则值为 0.00
     */
    @JSONField(name = "paid_coupon_fee")
    private String paidCouponFee;

    /**
     * 付款时间。格式:yyyy-MM-dd HH:mm:ss。订单的付款时间即为物流订单的创建时间。
     */
    @JSONField(name = "pay_time")
    private Date payTime;

    /**
     * 实付金额。精确到2位小数;单位:元。如:200.07，表示:200元7分
     */
    @JSONField(name = "payment")
    private String payment;

    /**
     * 天猫点券卡实付款金额,单位分
     */
    @JSONField(name = "pcc_af")
    private Long pccAf;

    /**
     * 商品图片绝对途径
     */
    @JSONField(name = "pic_path")
    private String picPath;

    /**
     * 买家使用积分,下单时生成，且一直不变。格式:100;单位:个.
     */
    @JSONField(name = "point_fee")
    private Long pointFee;

    /**
     * 邮费。精确到2位小数;单位:元。如:200.07，表示:200元7分
     */
    @JSONField(name = "post_fee")
    private String postFee;

    /**
     * 邮关订单
     */
    @JSONField(name = "post_gate_declare")
    private Boolean postGateDeclare;

    /**
     * 商品价格。精确到2位小数；单位：元。如：200.07，表示：200元7分
     */
    @JSONField(name = "price")
    private String price;

    /**
     * 交易促销详细信息
     */
    @JSONField(name = "promotion")
    private String promotion;

    /**
     * 买家实际使用积分（扣除部分退款使用的积分），交易完成后生成（交易成功或关闭），交易未完成时该字段值为0。格式:100;单位:个
     */
    @JSONField(name = "real_point_fee")
    private Long realPointFee;

    /**
     * 卖家实际收到的支付宝打款金额（由于子订单可以部分确认收货，这个金额会随着子订单的确认收货而不断增加，交易成功后等于买家实付款减去退款金额）。精确到2位小数;单位:元。如:200.07，表示:200元7分
     */
    @JSONField(name = "received_payment")
    private String receivedPayment;

    /**
     * 收货人的详细地址
     */
    @JSONField(name = "receiver_address")
    private String receiverAddress;

    /**
     * 收货人的所在城市<br/>注：因为国家对于城市和地区的划分的有：省直辖市和省直辖县级行政区（区级别的）划分的，淘宝这边根据这个差异保存在不同字段里面比如：广东广州：广州属于一个直辖市是放在的receiver_city的字段里面；而河南济源：济源属于省直辖县级行政区划分，是区级别的，放在了receiver_district里面<br/>建议：程序依赖于城市字段做物流等判断的操作，最好加一个判断逻辑：如果返回值里面只有receiver_district参数，该参数作为城市
     */
    @JSONField(name = "receiver_city")
    private String receiverCity;

    /**
     * 收货人国籍
     */
    @JSONField(name = "receiver_country")
    private String receiverCountry;

    /**
     * 收货人的所在地区<br/>注：因为国家对于城市和地区的划分的有：省直辖市和省直辖县级行政区（区级别的）划分的，淘宝这边根据这个差异保存在不同字段里面比如：广东广州：广州属于一个直辖市是放在的receiver_city的字段里面；而河南济源：济源属于省直辖县级行政区划分，是区级别的，放在了receiver_district里面<br/>建议：程序依赖于城市字段做物流等判断的操作，最好加一个判断逻辑：如果返回值里面只有receiver_district参数，该参数作为城市
     */
    @JSONField(name = "receiver_district")
    private String receiverDistrict;

    /**
     * 收货人的手机号码
     */
    @JSONField(name = "receiver_mobile")
    private String receiverMobile;

    /**
     * 收货人的姓名
     */
    @JSONField(name = "receiver_name")
    private String receiverName;

    /**
     * 收货人的电话号码
     */
    @JSONField(name = "receiver_phone")
    private String receiverPhone;

    /**
     * 收货人的所在省份
     */
    @JSONField(name = "receiver_state")
    private String receiverState;

    /**
     * 收货人街道地址
     */
    @JSONField(name = "receiver_town")
    private String receiverTown;

    /**
     * 收货人的邮编
     */
    @JSONField(name = "receiver_zip")
    private String receiverZip;

    /**
     * 处方药未审核状态
     */
    @JSONField(name = "rx_audit_status")
    private String rxAuditStatus;

    /**
     * 卖家支付宝账号
     */
    @JSONField(name = "seller_alipay_no")
    private String sellerAlipayNo;

    /**
     * 卖家是否可以对订单进行评价
     */
    @JSONField(name = "seller_can_rate")
    private Boolean sellerCanRate;

    /**
     * 卖家货到付款服务费。精确到2位小数;单位:元。如:12.07，表示:12元7分。卖家不承担服务费的订单：未发货的订单获取服务费为0，发货后就能获取到正确值。
     */
    @JSONField(name = "seller_cod_fee")
    private String sellerCodFee;

    /**
     * 卖家邮件地址
     */
    @JSONField(name = "seller_email")
    private String sellerEmail;

    /**
     * 卖家备注旗帜（与淘宝网上订单的卖家备注旗帜对应，只有卖家才能查看该字段）红、黄、绿、蓝、紫 分别对应 1、2、3、4、5
     */
    @JSONField(name = "seller_flag")
    private Long sellerFlag;

    /**
     * 卖家备注（与淘宝网上订单的卖家备注对应，只有卖家才能查看该字段）
     */
    @JSONField(name = "seller_memo")
    private String sellerMemo;

    /**
     * 卖家手机
     */
    @JSONField(name = "seller_mobile")
    private String sellerMobile;

    /**
     * 卖家姓名
     */
    @JSONField(name = "seller_name")
    private String sellerName;

    /**
     * 卖家昵称
     */
    @JSONField(name = "seller_nick")
    private String sellerNick;

    /**
     * 卖家电话
     */
    @JSONField(name = "seller_phone")
    private String sellerPhone;

    /**
     * 卖家是否已评价。可选值:true(已评价),false(未评价)
     */
    @JSONField(name = "seller_rate")
    private Boolean sellerRate;

    /**
     * 订单将在此时间前发出，主要用于预售订单
     */
    @JSONField(name = "send_time")
    private String sendTime;

    /**
     * serviceType
     */
    @JSONField(name = "service_type")
    private String serviceType;

    /**
     * shareGroupHold
     */
    @JSONField(name = "share_group_hold")
    private Long shareGroupHold;

    /**
     * 创建交易时的物流方式（交易完成前，物流方式有可能改变，但系统里的这个字段一直不变）。可选值：free(卖家包邮),post(平邮),express(快递),ems(EMS),virtual(虚拟发货)，25(次日必达)，26(预约配送)。
     */
    @JSONField(name = "shipping_type")
    private String shippingType;

    /**
     * 线下自提门店编码
     */
    @JSONField(name = "shop_code")
    private String shopCode;

    /**
     * 门店自提，总店发货，分店取货的门店自提订单标识
     */
    @JSONField(name = "shop_pick")
    private String shopPick;

    /**
     * 物流运单号
     */
    @JSONField(name = "sid")
    private String sid;

    /**
     * 交易快照详细信息
     */
    @JSONField(name = "snapshot")
    private String snapshot;

    /**
     * 交易快照地址
     */
    @JSONField(name = "snapshot_url")
    private String snapshotUrl;

    /**
     * 交易状态。可选值: * TRADE_NO_CREATE_PAY(没有创建支付宝交易) * WAIT_BUYER_PAY(等待买家付款) * SELLER_CONSIGNED_PART(卖家部分发货) * WAIT_SELLER_SEND_GOODS(等待卖家发货,即:买家已付款) * WAIT_BUYER_CONFIRM_GOODS(等待买家确认收货,即:卖家已发货) * TRADE_BUYER_SIGNED(买家已签收,货到付款专用) * TRADE_FINISHED(交易成功) * TRADE_CLOSED(付款以后用户退款成功，交易自动关闭) * TRADE_CLOSED_BY_TAOBAO(付款以前，卖家或买家主动关闭交易) * PAY_PENDING(国际信用卡支付付款确认中) * WAIT_PRE_AUTH_CONFIRM(0元购合约中)	* PAID_FORBID_CONSIGN(拼团中订单，已付款但禁止发货)
     */
    @JSONField(name = "status")
    private String status;

    /**
     * 分阶段付款的已付金额（万人团订单已付金额）
     */
    @JSONField(name = "step_paid_fee")
    private String stepPaidFee;

    /**
     * 分阶段付款的订单状态（例如万人团订单等），目前有三返回状态FRONT_NOPAID_FINAL_NOPAID(定金未付尾款未付)，FRONT_PAID_FINAL_NOPAID(定金已付尾款未付)，FRONT_PAID_FINAL_PAID(定金和尾款都付)
     */
    @JSONField(name = "step_trade_status")
    private String stepTradeStatus;

    /**
     * 天猫拼团拦截标示
     */
    @JSONField(name = "team_buy_hold")
    private Long teamBuyHold;

    /**
     * 交易编号 (父订单的交易编号)
     */
    @JSONField(name = "tid")
    private Long tid;

    /**
     * 同tid
     */
    @JSONField(name = "tid_str")
    private String tidStr;

    /**
     * 超时到期时间。格式:yyyy-MM-dd HH:mm:ss。业务规则：前提条件：只有在买家已付款，卖家已发货的情况下才有效如果申请了退款，那么超时会落在子订单上；比如说3笔ABC，A申请了，那么返回的是BC的列表, 主定单不存在如果没有申请过退款，那么超时会挂在主定单上；比如ABC，返回主定单，ABC的超时和主定单相同
     */
    @JSONField(name = "timeout_action_time")
    private Date timeoutActionTime;

    /**
     * 交易标题，以店铺名作为此标题的值。注:taobao.trades.get接口返回的Trade中的title是商品名称
     */
    @JSONField(name = "title")
    private String title;

    /**
     * TOP拦截标识，0不拦截，1拦截
     */
    @JSONField(name = "top_hold")
    private Long topHold;

    /**
     * top定义订单类型
     */
    @JSONField(name = "toptype")
    private Long toptype;

    /**
     * 商品金额（商品价格乘以数量的总金额）。精确到2位小数;单位:元。如:200.07，表示:200元7分
     */
    @JSONField(name = "total_fee")
    private String totalFee;

    /**
     * top动态字段
     */
    @JSONField(name = "trade_attr")
    private String tradeAttr;

    /**
     * 交易内部来源。WAP(手机);HITAO(嗨淘);TOP(TOP平台);TAOBAO(普通淘宝);JHS(聚划算)一笔订单可能同时有以上多个标记，则以逗号分隔
     */
    @JSONField(name = "trade_from")
    private String tradeFrom;

    /**
     * 交易备注。
     */
    @JSONField(name = "trade_memo")
    private String tradeMemo;

    /**
     * 交易外部来源：ownshop(商家官网)
     */
    @JSONField(name = "trade_source")
    private String tradeSource;

    /**
     * 交易类型列表，同时查询多种交易类型可用逗号分隔。默认同时查询guarantee_trade, auto_delivery, ec, cod的4种交易类型的数据 可选值 fixed(一口价) auction(拍卖) guarantee_trade(一口价、拍卖) auto_delivery(自动发货) independent_simple_trade(旺店入门版交易) independent_shop_trade(旺店标准版交易) ec(直冲) cod(货到付款) fenxiao(分销) game_equipment(游戏装备) shopex_trade(ShopEX交易) netcn_trade(万网交易) external_trade(统一外部交易)o2o_offlinetrade（O2O交易）step (万人团)nopaid(无付款订单)pre_auth_type(预授权0元购机交易)
     */
    @JSONField(name = "type")
    private String type;

    /**
     * 订单的运费险，单位为元
     */
    @JSONField(name = "yfx_fee")
    private String yfxFee;

    /**
     * 运费险支付号
     */
    @JSONField(name = "yfx_id")
    private String yfxId;

    /**
     * 运费险类型
     */
    @JSONField(name = "yfx_type")
    private String yfxType;

    /**
     * 在返回的trade对象上专门增加一个字段zero_purchase来区分，这个为true的就是0元购机预授权交易
     */
    @JSONField(name = "zero_purchase")
    private Boolean zeroPurchase;

}
