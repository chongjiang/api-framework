package com.mgy.api.framework.common.enums;


/**
 * Person
 *
 * @author mgy
 * @date 2019.10.20
 */
public class Platform {
    /**
     * 淘宝
     */
    public static final String TB = "tb";

    /**
     * 拼多多
     */
    public static final String PDD = "pdd";

    /**
     * 京东
     */
    public static final String JD = "jd";

}
