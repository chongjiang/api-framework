package com.mgy.api.framework.service.impl;

import com.mgy.api.framework.common.domain.Order;
import com.mgy.api.framework.common.domain.Trade;
import com.mgy.api.framework.common.enums.Platform;
import com.mgy.api.framework.service.TradeService;
import com.mgy.api.framework.core.annotation.Adapter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Adapter(Platform.TB)
@Service("tbTradeService")
public class TbTradeServiceImpl implements TradeService {
    @Override
    public Trade getTrade2(String tid) {
        Trade trade = new Trade();
        trade.setTid(123467657564L);
        trade.setPayment("333");
        trade.setPicPath("http://www.taobao.com/a/b/img.jpg");
        trade.setSellerNick("淘宝卖家昵称33");
        trade.setBuyerNick("淘宝买家昵称33");
        trade.setTitle("淘宝测试商品2");
        trade.setNum(44L);
        trade.setNumIid(4444444L);

        List<Order> orders = new ArrayList<>();
        Order order = new Order();
        order.setOid(13243234234L);
        order.setPayment("12");
        order.setSkuPropertiesName("红色；XXL");
        order.setSkuId("3423wer");
        order.setOuterSkuId("规格商家编码");
        orders.add(order);
        trade.setOrders(orders);
        return trade;
    }

    @Override
    public Trade getTrade(String tid) {
        Trade trade = new Trade();
        trade.setTid(123467657564L);
        trade.setPayment("128");
        trade.setPicPath("http://www.taobao.com/a/b/img.jpg");
        trade.setSellerNick("淘宝卖家昵称");
        trade.setBuyerNick("淘宝买家昵称");
        trade.setTitle("淘宝测试商品");
        trade.setNum(3L);
        trade.setNumIid(3543534L);

        List<Order> orders = new ArrayList<>();
        Order order = new Order();
        order.setOid(13243234234L);
        order.setPayment("12");
        order.setSkuPropertiesName("红色；XXL");
        order.setSkuId("3423wer");
        order.setOuterSkuId("规格商家编码");
        orders.add(order);
        trade.setOrders(orders);
        return trade;
    }
}
