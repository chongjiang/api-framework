package com.mgy.api.framework.service.impl;

import com.mgy.api.framework.common.domain.Order;
import com.mgy.api.framework.common.domain.Trade;
import com.mgy.api.framework.common.enums.Platform;
import com.mgy.api.framework.service.TradeService;
import com.mgy.api.framework.core.annotation.Adapter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Adapter({Platform.PDD, Platform.JD})
@Service("pddTradeService")
public class PddTradeServiceImpl implements TradeService {
    @Override
    public Trade getTrade(String tid) {
        Trade trade = new Trade();
        trade.setTid(1113467657564L);
        trade.setPayment("128");
        trade.setPicPath("http://www.pdd.com/a/b/img.jpg");
        trade.setSellerNick("拼多多卖家昵称");
        trade.setBuyerNick("拼多多买家昵称");
        trade.setTitle("拼多多测试商品");
        trade.setNum(2L);
        trade.setNumIid(35411113534L);

        List<Order> orders = new ArrayList<>();
        Order order = new Order();
        order.setOid(888243234234L);
        order.setPayment("12");
        order.setSkuPropertiesName("红色；XXL");
        order.setSkuId("3423wer");
        order.setOuterSkuId("规格商家编码");
        orders.add(order);
        trade.setOrders(orders);
        return trade;
    }

    @Override
    public Trade getPinTuanTrade(String tid) {
        Trade trade = new Trade();
        trade.setTid(55553467657564L);
        trade.setPayment("128");
        trade.setPicPath("http://www.pdd.com/a/b/img.jpg");
        trade.setSellerNick("拼多多卖家昵称");
        trade.setBuyerNick("拼多多买家昵称");
        trade.setTitle("拼多多拼团订单测试");
        trade.setNum(2L);
        trade.setNumIid(666635411113534L);
        return trade;
    }


    @Override
    public Trade getTrade2(String tid) {
        Trade trade = new Trade();
        trade.setTid(1113467657564L);
        trade.setPayment("1282");
        trade.setPicPath("http://www.pdd.com/a/b/img.jpg");
        trade.setSellerNick("拼多多卖家昵称2");
        trade.setBuyerNick("拼多多买家昵称2");
        trade.setTitle("拼多多测试商品2");
        trade.setNum(2L);
        trade.setNumIid(35411113534L);

        List<Order> orders = new ArrayList<>();
        Order order = new Order();
        order.setOid(888243234234L);
        order.setPayment("12");
        order.setSkuPropertiesName("红色；XXL2");
        order.setSkuId("3423wer");
        order.setOuterSkuId("规格商家编码2");
        orders.add(order);
        trade.setOrders(orders);
        return trade;
    }
}
