package com.mgy.api.framework.service;

import com.mgy.api.framework.common.domain.Trade;

/**
 * TradeService
 *
 * @author mgy
 * @date 2019.10.20
 */
public interface TradeService {
    Trade getTrade(String tid);

    default Trade getTrade2(String tid) {
        return null;
    }


    default Trade getPinTuanTrade(String tid) {
        return null;
    }
}
