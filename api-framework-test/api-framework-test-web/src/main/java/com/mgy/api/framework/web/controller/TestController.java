package com.mgy.api.framework.web.controller;

import com.mgy.api.framework.common.domain.Person;
import com.mgy.api.framework.common.domain.Trade;
import com.mgy.api.framework.core.annotation.ApiController;
import com.mgy.api.framework.core.annotation.IgnoreWrap;
import com.mgy.api.framework.core.api.ApiResponse;
import com.mgy.api.framework.service.TradeService;
import com.mgy.api.framework.core.adapter.AdapterFactory;
import com.mgy.api.framework.core.annotation.Action;
import io.swagger.annotations.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * controller
 *
 * @author mgy
 * @date 2019.10.20
 */
@Api(value = "测试接口", tags = "Test接口信息", description = "测试相关接口")
@ApiController
public class TestController {
    //@ApiParam  参数注解
    @ApiImplicitParams({
            @ApiImplicitParam(name = "platform", value = "平台", required = true, paramType = "query",example = "tb"),
            @ApiImplicitParam(name = "tid", value = "订单号", required = true, paramType = "query",example = "1111")

    })
    @ApiOperation(value = "查询订单", httpMethod = "POST", response = Trade.class, notes = "查询订单note")
    @Action(value = "mgy.trade.get")
    public Object getTrade(String platform, String tid) {
        TradeService tradeService = AdapterFactory.getInstance(TradeService.class, platform);
        return tradeService.getTrade(tid);
    }


    @ApiImplicitParams({
            @ApiImplicitParam(name = "version", value = "版本号", allowableValues = "1.0,2.0", paramType = "query"),
            @ApiImplicitParam(name = "platform", value = "平台", required = true, paramType = "query",example = "pdd"),
            @ApiImplicitParam(name = "tid", value = "订单号", required = true, paramType = "query",example = "2222")
    })
    @ApiOperation(value = "查询订单", httpMethod = "POST", response = Trade.class, notes = "查询订单note")
    @IgnoreWrap
    @Action(value = "mgy.trade.get", version = "2.0")
    public Object getTrade2(String platform, String tid) {
        TradeService tradeService = AdapterFactory.getInstance(TradeService.class, platform);
        return tradeService.getTrade2(tid);
    }


    @ApiImplicitParams({
            @ApiImplicitParam(name = "method", value = "mgy.api.test.error", required = true, paramType = "query"),
            @ApiImplicitParam(name = "version", value = "版本号", allowableValues = "1.0,2.0", paramType = "query")

    })
    @ApiOperation(value = "错误接口测试", httpMethod = "GET", response = ApiResponse.class, notes = "测试错误接口")
    @Action("mgy.api.test.error")
    public Object testError() {
        throw new RuntimeException("测试接口失败");
    }

    @ApiImplicitParams({
            @ApiImplicitParam(name = "method", value = "mgy.trades.sold.get", required = true, paramType = "query"),
            @ApiImplicitParam(name = "version", value = "版本号", allowableValues = "1.0,2.0", paramType = "query"),

    })
    @ApiOperation(value = "订单列表获取", httpMethod = "POST", response = Person.class, notes = "订单列表获取")
    @Action("mgy.trades.sold.get")
    public Object apiTest2( Person person) {
        return person;
    }


    @ApiImplicitParams({
            @ApiImplicitParam(name = "method", value = "mgy.trade.memo.add", required = true, paramType = "query"),
            @ApiImplicitParam(name = "version", value = "版本号", allowableValues = "1.0,2.0", paramType = "query"),

    })
    @ApiOperation(value = "添加备注", httpMethod = "POST", response = Person.class, notes = "添加备注测试")
    @Action("mgy.trade.memo.add")
    public Object apiTest3(@RequestBody Person person) {
        return person;
    }

}
