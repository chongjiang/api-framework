package com.mgy.api.framework.web.controller;

import com.mgy.api.framework.common.domain.Trade;
import com.mgy.api.framework.core.adapter.AdapterFactory;
import com.mgy.api.framework.core.annotation.Action;
import com.mgy.api.framework.service.TradeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Api(value = "测试接口", tags = "Test2接口信息", description = "测试相关接口2")
@RequestMapping("/test2")
@Controller
public class Test2Controller {

    //@ApiParam  参数注解
    @ApiImplicitParams({
            @ApiImplicitParam(name = "platform", value = "平台", required = true, paramType = "form"),
            @ApiImplicitParam(name = "tid", value = "订单号", required = true, paramType = "form"),

    })
    @ApiOperation(value = "查询订单2", httpMethod = "POST", response = Trade.class)
    @ResponseBody
    @RequestMapping("/getTrade")
    public Trade getTrade(String platform, String tid) {
        TradeService tradeService = AdapterFactory.getInstance(TradeService.class, platform);
        return tradeService.getTrade(tid);
    }
}
