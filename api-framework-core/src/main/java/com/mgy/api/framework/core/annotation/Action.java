package com.mgy.api.framework.core.annotation;

import org.springframework.core.annotation.AliasFor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.lang.annotation.*;

/**
 * 标注在控制器中方法的注解
 *
 * @author mgy
 * @date 2019.10.20
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@RequestMapping
@ResponseBody
public @interface Action {
    /**
     * api的名称,例如taobao.trades.sold.get
     */
    String value();

    /**
     * api的版本
     */
    String version() default "";

    String name() default "";


    String[] headers() default {};

    String[] consumes() default {};

    String[] produces() default {};

    RequestMethod[] method() default {};

    /**
     * 接口说明
     */
    String description() default "无";
}
