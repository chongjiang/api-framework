package com.mgy.api.framework.core.annotation;

import java.lang.annotation.*;

/**
 * 不包装结果
 *
 * @author mgy
 * @date 2019.10.20
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IgnoreWrap {
}
