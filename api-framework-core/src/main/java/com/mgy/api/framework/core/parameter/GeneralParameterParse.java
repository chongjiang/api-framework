package com.mgy.api.framework.core.parameter;

import lombok.extern.slf4j.Slf4j;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * 方法参数解析
 *
 * @author mgy
 * @date 2019.10.20
 */
@Slf4j
public class GeneralParameterParse extends AbstractParameterParse {


    public GeneralParameterParse() {

    }

    public GeneralParameterParse(HttpServletRequest request, HttpServletResponse response) {
        super(request, response);
    }

    @Override
    protected MultipartFile getMultipartFile() {
        return null;
    }

    @Override
    protected MultipartHttpServletRequest getMultipartHttpServletRequest() {
        return null;
    }

}
