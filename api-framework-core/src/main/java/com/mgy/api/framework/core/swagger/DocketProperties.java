package com.mgy.api.framework.core.swagger;

import com.google.common.collect.Sets;
import lombok.Data;
import org.springframework.http.MediaType;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.*;

/**
 * swagger配置
 *
 * @author mgy
 * @date 2019.11.13
 */
@Data
public class DocketProperties {

    /**
     * 分组名称(swagger-ui页面右上角下拉框栏目)
     */
    private String groupName;
    /**
     * controller package
     */
    private String basePackage;

    private Boolean useDefaultResponseMessages;
    /**
     * 协议,默认[http,https]
     */
    private Set<String> protocols = Sets.newHashSet("http", "https");
    /**
     * 返回结构内容类型集合(Content-Type)
     * MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE
     */
    private Set<String> produces = Sets.newHashSet();
    /**
     * 提交参数内容类型集合(Content-Type)
     * MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE
     */
    private Set<String> consumes = Sets.newHashSet();

    private ApiInfoProperties apiInfo = new ApiInfoProperties();

    Docket build() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo.build())
                .select()
                //RequestHandlerSelectors.any()
                .apis(RequestHandlerSelectors.basePackage(this.basePackage))
                .paths(PathSelectors.any())
                .build()
                .useDefaultResponseMessages(this.useDefaultResponseMessages)
                .groupName(this.groupName)
                .protocols(this.protocols)
                .produces(this.produces)
                .consumes(this.consumes);


    }

}
