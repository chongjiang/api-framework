package com.mgy.api.framework.core.swagger;

import lombok.Data;
import springfox.documentation.service.Contact;

/**
 * swagger配置
 *
 * @author mgy
 * @date 2019.11.13
 */
@Data
public class ContactProperties {
    private String name = "";
    private String url = "";
    private String email = "";

    Contact build() {
        return new Contact(name, url, email);
    }
}
