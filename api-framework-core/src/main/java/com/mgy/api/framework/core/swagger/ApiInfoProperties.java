package com.mgy.api.framework.core.swagger;

import lombok.Data;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;

/**
 * swagger配置
 *
 * @author mgy
 * @date 2019.11.13
 */
@Data
public class ApiInfoProperties {
    private String version;
    private String title;
    private String description;
    private String termsOfServiceUrl;
    private String license;
    private String licenseUrl;
    private ContactProperties contact = new ContactProperties();

    ApiInfo build() {
        return new ApiInfoBuilder()
                ////标题
                .title(this.title)
                //描述
                .description(this.description)
                .version(this.version)
                //（不可见）条款地址，公司内部使用的话不需要配
                .termsOfServiceUrl(this.termsOfServiceUrl)
                ////作者信息
                .contact(contact.build())
                .license(this.license)
                .licenseUrl(this.licenseUrl)
                .build();
    }
}
