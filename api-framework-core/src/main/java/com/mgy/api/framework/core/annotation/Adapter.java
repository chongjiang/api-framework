package com.mgy.api.framework.core.annotation;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * Adapter
 *
 * @author mgy
 * @date 2019.10.20
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Adapter {
    @AliasFor(attribute = "platform")
    String[] value() default {};

    @AliasFor(attribute = "value")
    String[] platform() default {};
}
