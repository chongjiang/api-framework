package com.mgy.api.framework.core.parameter;

import lombok.extern.slf4j.Slf4j;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 方法参数解析 multipart/form-data
 *
 * @author mgy
 * @date 2019.10.20
 */
@Slf4j
public class FormDataParameterParse extends AbstractParameterParse {

    private MultipartHttpServletRequest multipartRequest;

    public FormDataParameterParse() {
        super();
    }

    public FormDataParameterParse(HttpServletRequest request, HttpServletResponse response) {
        super(request, response);
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartRequest = multipartResolver.resolveMultipart(request);
    }

    @Override
    protected MultipartFile getMultipartFile() {
        for (Map.Entry<String, List<MultipartFile>> entry : multipartRequest.getMultiFileMap().entrySet()) {
            return entry.getValue().get(0);
        }
        return null;
    }

    @Override
    protected MultipartHttpServletRequest getMultipartHttpServletRequest() {
        return multipartRequest;
    }


}
