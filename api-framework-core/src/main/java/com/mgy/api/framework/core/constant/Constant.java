package com.mgy.api.framework.core.constant;

/**
 * 通用常量,参照淘宝api命名
 *
 * @author mgy
 * @date 2019.10.20
 */
public class Constant {
    public static final String APP_KEY = "app_key";
    public static final String FORMAT = "format";
    public static final String METHOD = "method";
    public static final String TIMESTAMP = "timestamp";
    public static final String VERSION = "v";
    public static final String SIGN = "sign";
}
