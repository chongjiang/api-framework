package com.mgy.api.framework.core.handler;

import com.mgy.api.framework.core.swagger.SwaggerProperties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 配置
 *
 * @author mgy
 * @date 2019.11.13
 */
@Data
@ConfigurationProperties(prefix = "api.framework")
public class ApiFrameworkProperties {


    /**
     * 根路径 /router/rest/api
     */
    private String rootPath;

    /**
     * 是否启用
     */
    private boolean enable = false;

    private SwaggerProperties swagger;
}
