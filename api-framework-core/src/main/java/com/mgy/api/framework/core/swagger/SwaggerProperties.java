package com.mgy.api.framework.core.swagger;

import lombok.Data;

/**
 * swagger配置
 *
 * @author mgy
 * @date 2019.11.15
 */
@Data
public class SwaggerProperties {
    private boolean enable = false;
    private DocketProperties docket;
}
