package com.mgy.api.framework.core.api;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * WebResponse
 *
 * @author mgy
 * @date 2019.10.20
 */
@Data
public class ApiResponse implements Serializable {


    @JSONField(name = "code")
    private String errorCode;

    @JSONField(name = "msg")
    private String msg;

    @JSONField(name = "sub_code")
    private String subCode;

    @JSONField(name = "sub_msg")
    private String subMsg;

    /**
     * API响应JSON或XML串
     */
    private String body;
    /**
     * API请求参数列表
     */
    private Map<String, String> params;

    public boolean isSuccess() {
        return this.errorCode == null && this.subCode == null;
    }

}
