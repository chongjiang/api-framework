package com.mgy.api.framework.core.aop;

import com.mgy.api.framework.core.annotation.Action;
import com.mgy.api.framework.core.annotation.IgnoreWrap;
import com.mgy.api.framework.core.api.ApiResponse;
import com.mysql.cj.jdbc.exceptions.MySQLTimeoutException;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.stereotype.Component;

import org.springframework.web.servlet.NoHandlerFoundException;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * 切面
 *
 * @author mgy
 * @date 2019.11.13
 */
@Slf4j
@Configuration
@Aspect
public class ActionAspect {

    @Pointcut("@annotation(com.mgy.api.framework.core.annotation.Action)")
    public void actionPointcut() {

    }

    @Around("actionPointcut()&&@annotation(action)")
    public Object around(ProceedingJoinPoint joinPoint, Action action) throws Throwable {
        try {
            MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
            Method method = methodSignature.getMethod();
            Object result = joinPoint.proceed();
            if (method.getAnnotation(IgnoreWrap.class) != null) {
                return result;
            }
            Map<String, Object> resultMap = new HashMap<>(6);
            resultMap.put(getResponseName(action.value()), result);
            return resultMap;
        } catch (IllegalArgumentException e) {
            log.error(e.getMessage(), e);
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.setErrorCode("201");
            apiResponse.setMsg("参数错误发生错误！" + e.getMessage());
            apiResponse.setSubMsg(e.getCause() == null ? "" : e.getCause().toString());
            return buildApiResponse(apiResponse);
        } catch (NullPointerException e) {
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.setErrorCode("301");
            log.error(e.getMessage(), e);
            apiResponse.setMsg("程序发生空指针错误！" + e.getMessage());
            apiResponse.setSubMsg(e.getCause() == null ? "" : e.getCause().toString());
            return buildApiResponse(apiResponse);
        } catch (DataAccessException e) {
            log.error(e.getMessage(), e);
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.setErrorCode("302");
            apiResponse.setMsg("数据更新发生错误！" + e.getMessage());
            apiResponse.setSubMsg(e.getCause() == null ? "" : e.getCause().toString());
            return buildApiResponse(apiResponse);
        } catch (NoHandlerFoundException e) {
            log.error(e.getMessage(), e);
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.setErrorCode("404");
            apiResponse.setMsg("请求资源不存在！" + e.getMessage());
            apiResponse.setSubMsg(e.getCause() == null ? "" : e.getCause().toString());
            return buildApiResponse(apiResponse);
        } catch (Exception e) {
            ApiResponse apiResponse = new ApiResponse();
            log.error(e.getMessage(), e);
            if (e instanceof UncategorizedSQLException) {
                if (e.getCause() != null && e.getCause().getCause() instanceof MySQLTimeoutException) {
                    apiResponse.setErrorCode("301");
                    apiResponse.setMsg("数据库连接超时！" + e.getMessage());
                    apiResponse.setSubMsg(e.getCause() == null ? "" : e.getCause().toString());
                    return apiResponse;
                }
            }
            apiResponse.setErrorCode("300");
            apiResponse.setMsg(e.getMessage());
            apiResponse.setSubMsg(e.getCause() == null ? "" : e.getCause().toString());
            return buildApiResponse(apiResponse);
        }
    }


    private Map<String, ApiResponse> buildApiResponse(ApiResponse apiResponse) {
        Map<String, ApiResponse> responseMap = new HashMap<>(1);
        responseMap.put("error_response", apiResponse);
        return responseMap;
    }

    public String getResponseName(String method) {
        return method.replace(".", "_") + "_response";
    }

}