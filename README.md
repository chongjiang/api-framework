## 本项目仅为测试项目，用于学习探讨技术，不涉及任何侵权及隐私

### 注意事项：

如果不使用spring boot对webmvc的自动配置的话（使用@EnableWebMvc注解或继承类WebMvcConfigurationSupport相当于放弃webmvc的自动配置），相关配置需要自己重写WebMvcConfigurationSupport中的方法自己实现。

1、针对本节的影响需要自己重写WebMvcConfigurationSupport的以下方法：
@Autowired
private ApiFrameworkProperties properties;
 
 
@Override
protected RequestMappingHandlerMapping createRequestMappingHandlerMapping() {
    return new CustomRequestMappingHandlerMapping(properties);
}
 
@Override
public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("swagger-ui.html")
            .addResourceLocations("classpath:/META-INF/resources/swagger-ui.html");
    registry.addResourceHandler("/webjars/**")
            .addResourceLocations("classpath:/META-INF/resources/webjars/");
}
 
@Override
protected void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
    super.configureMessageConverters(converters);
    FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
    FastJsonConfig fastJsonConfig = new FastJsonConfig();
    fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat);
    List<MediaType> fastMediaTypes = new ArrayList<>();
    fastMediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
    fastConverter.setSupportedMediaTypes(fastMediaTypes);
    fastConverter.setFastJsonConfig(fastJsonConfig);
    converters.add(fastConverter);
}

所以还是建议使用webmvc的自动配置。

2、如果使用的是spring boot对webmvc的自动配置，但是打开swagger页面显示404的话，可以尝试一下操作，
  重写资源的处理

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //第一个方法设置访问路径前缀，第二个方法设置资源路径

        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/swagger-ui.html");
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}
